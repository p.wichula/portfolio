# Portfolio (Przemysław Wichuła)

This project was generated with [Angular CLI] version 16.2.0.
* To run the application, make sure you have Node.js installed on your machine.
* Navigate to the project directory
* Install the dependencies: `npm install`
* Start the development server: `npm start`
* Open your browser and navigate to `http://localhost:4201/`

## Description
* The application uses endpoint for searching lyrics for specific author and song name.
* The application requires to input username (this part will be implemented in future - stored in LocalStorage and used to display greetings).
* The application has light mode (default) and dark mode,
* The application uses Store Service which is the only source of truth and keep information across whole application
* The application uses CSS variables for dynamically changing modes and SCSS variables for static values,
* The application uses Auth Guard to prevent user move to dashboard without input the username - in currently state of the application it is pointless but in further implementations it is required,
* The application is intended to meet web accessibility requirements, the colors for the light mode have been selected in accordance with the requirements.
* The application is optimized with OnPush strategy,
* The application uses old way for generating routes and modules (no standalone components),
* The application uses Angular Material icons,

## TODOS
* Add tests for dashboard and login components,
* Add LocalStorage to store username and already searched songs,
* Add aria-labels and tab-indexes,
* Add translation pipe and translations for texts,
* Add enums for Store actions,
* Add facade for easier setting new values for AppStore,

## Code coverage

Run `npm test -- --code-coverage` to generate code coverage report. The report will be available in the `coverage` folder.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4201/`. The application will automatically reload if you change any of the source files.
