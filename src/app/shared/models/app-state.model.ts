import { THEMES } from "../enums/themes.enum";

export interface AppState {
  user: {
    name?: string;
    favourites: string[];
  },
  uiState: {
    theme: THEMES;
    pending: boolean;
    disableEverything: boolean;
  }
}
