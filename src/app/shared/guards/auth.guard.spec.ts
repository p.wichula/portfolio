import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { StoreService } from '../../core/services/store.service';
import { AppState } from '../../shared/models/app-state.model';
import { THEMES } from "../enums/themes.enum";

class MockStoreService {
  private state: AppState = {
    user: {
      name: undefined,
      favourites: []
    },
    uiState: {
      theme: THEMES.LIGHT,
      pending: false,
      disableEverything: false
    }
  };

  getState(): AppState {
    return this.state;
  }

  setState(state: Partial<AppState>): void {
    this.state = { ...this.state, ...state };
  }
}

class MockRouter {
  navigate(path: string[]): void {}
}

describe('AuthGuard', () => {
  let authGuard: AuthGuard;
  let storeService: MockStoreService;
  let router: MockRouter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        { provide: StoreService, useClass: MockStoreService },
        { provide: Router, useClass: MockRouter }
      ]
    });

    authGuard = TestBed.inject(AuthGuard);
    storeService = TestBed.inject(StoreService) as unknown as MockStoreService;
    router = TestBed.inject(Router) as unknown as MockRouter;
  });

  it('should be created', () => {
    expect(authGuard).toBeTruthy();
  });

  it('should allow activation if user is logged in', () => {
    storeService.setState({ user: { name: 'John Doe' } } as Partial<AppState>);
    expect(authGuard.canActivate()).toBeTrue();
  });

  it('should not allow activation and navigate to login if user is not logged in', () => {
    spyOn(router, 'navigate');
    storeService.setState({ user: { name: undefined } } as Partial<AppState>);
    expect(authGuard.canActivate()).toBeFalse();
    expect(router.navigate).toHaveBeenCalledWith(['/login']);
  });
});
