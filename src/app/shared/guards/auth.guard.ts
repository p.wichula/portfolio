import { inject, Injectable } from "@angular/core";
import { StoreService } from "../../core/services/store.service";
import { CanActivateFn, Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {
  constructor(private _store: StoreService, private _router: Router) {
  }

  /**
   * Checks if the user is authorized.
   *
   * @returns {boolean} Returns true if the user has a name, otherwise it redirects the user to /login and returns false.
   */
  canActivate(): boolean {
    if (this._store.getState().user.name) {
      return true;
    } else {
      this._router.navigate(['/login']);
      return false;
    }
  }
}

export const AuthorizationGuard: CanActivateFn = (): boolean => {
  return inject(AuthGuard).canActivate();
}
