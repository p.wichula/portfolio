import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputFieldComponent } from './input-field.component';
import { FormsModule, ReactiveFormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SUPPORTIVE_TEXTS } from '../../enums/supportive-texts.enum';
import { FIELD_TYPES } from '../../enums/field-types.enum';
import { By } from '@angular/platform-browser';
import { forwardRef } from "@angular/core";

describe('InputFieldComponent', () => {
  let component: InputFieldComponent;
  let fixture: ComponentFixture<InputFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InputFieldComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => InputFieldComponent),
          multi: true
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have default input properties', () => {
    expect(component.label).toBe('');
    expect(component.type).toBe(FIELD_TYPES.TEXT);
    expect(component.placeholder).toBe('');
    expect(component.errorMsg).toBe(SUPPORTIVE_TEXTS.INVALID_VALUE);
    expect(component.error).toBeFalse();
    expect(component.disabled).toBeFalse();
  });

  it('should write value', () => {
    component.writeValue('Test value');
    expect(component.value).toBe('Test value');
  });

  it('should register onChange function', () => {
    const fn = jasmine.createSpy('onChange');
    component.registerOnChange(fn as any);
    component.changed('New value');
    expect(fn).toHaveBeenCalledWith('New value');
  });

  it('should register onTouched function', () => {
    const fn = jasmine.createSpy('onTouched');
    component.registerOnTouched(fn as any);
    component.touched();
    expect(fn).toHaveBeenCalled();
  });

  it('should call changed and touched on input event', () => {
    const changedSpy = spyOn(component, 'changed');
    const touchedSpy = spyOn(component, 'touched');

    const inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    inputElement.value = 'New input';
    inputElement.dispatchEvent(new Event('input'));

    expect(changedSpy).toHaveBeenCalledWith('New input');
    expect(touchedSpy).toHaveBeenCalled();
  });

  it('should render input with proper attributes', () => {
    component.label = 'Test Label';
    component.maxLength = 10;
    component.type = FIELD_TYPES.TEXT;
    component.placeholder = 'Enter text';
    component.errorMsg = SUPPORTIVE_TEXTS.INVALID_VALUE;
    component.error = true;
    component.disabled = true;
    fixture.detectChanges();

    const inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    expect(inputElement.getAttribute('maxLength')).toBe('10');
    expect(inputElement.type).toBe(FIELD_TYPES.TEXT);
    expect(inputElement.placeholder).toBe('Enter text');
    expect(inputElement.disabled).toBeTrue();
  });
});
