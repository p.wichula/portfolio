import { ChangeDetectionStrategy, Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { SUPPORTIVE_TEXTS } from "../../enums/supportive-texts.enum";
import { FIELD_TYPES } from "../../enums/field-types.enum";

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputFieldComponent),
    multi: true,
  }],
})
export class InputFieldComponent {
  @Input() label = '';
  @Input() maxLength?: number;
  @Input() type = FIELD_TYPES.TEXT;
  @Input() placeholder = '';
  @Input() errorMsg = SUPPORTIVE_TEXTS.INVALID_VALUE;
  @Input() error = false;
  @Input() disabled = false;

  value?: string;

  // eslint-disable-next-line
  changed: any = (value: any) => value;
  // eslint-disable-next-line
  touched: any = () => {};

  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(fn: string): void {
    this.changed = fn;
  }

  registerOnTouched(fn: string): void {
    this.touched = fn;
  }

  onInput(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.changed(target.value);
    this.touched();
  }

  protected readonly SUPPORTIVE_TEXTS = SUPPORTIVE_TEXTS;
}
