export enum SUPPORTIVE_TEXTS {
  /** Inputs utils **/
  INVALID_VALUE = "Invalid value",
  DOTS = "...",

  /** FormFields labels **/
  ARTIST_NAME = "Artist name",
  SONG_TITLE = "Song title",
  NAME = "Name",
}
