import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { InputFieldComponent } from './shared/components/input-field/input-field.component';
import { RouterOutlet } from "@angular/router";
import { DashboardComponent } from './core/components/dashboard/dashboard.component';
import { LoginComponent } from './core/components/login/login.component';
import { AppRoutingModule } from "./app-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { ThemeChangerComponent } from './core/components/theme-changer/theme-changer.component';

@NgModule({
  declarations: [
    AppComponent,
    InputFieldComponent,
    DashboardComponent,
    LoginComponent,
    ThemeChangerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterOutlet,
    AppRoutingModule,
    ReactiveFormsModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
