import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, Renderer2, RendererFactory2 } from '@angular/core';
import { StoreService } from "./core/services/store.service";
import { THEMES } from "./shared/enums/themes.enum";
import { Observable, Subject, takeUntil, tap } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {
  theme$: Observable<THEMES> = this._store.theme$;

  private _renderer: Renderer2;
  private _colorSchemeAttrName = 'color-scheme';
  private _destroy$ = new Subject<void>();

  constructor(private _store: StoreService, private _rendererFactory: RendererFactory2) {
    this._renderer = this._rendererFactory.createRenderer(null, null);
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  ngOnInit(): void {
    this.theme$.pipe(
      takeUntil(this._destroy$),
      tap((value: THEMES) => {
        this._renderer.setAttribute(document.firstElementChild, this._colorSchemeAttrName, value);
      })
    ).subscribe()
  }
}
