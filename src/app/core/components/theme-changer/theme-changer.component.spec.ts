import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ThemeChangerComponent } from './theme-changer.component';
import { StoreService } from '../../services/store.service';
import { THEMES } from '../../../shared/enums/themes.enum';
import { BehaviorSubject } from 'rxjs';
import { AppState } from '../../../shared/models/app-state.model';
import { MatIconModule } from '@angular/material/icon';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { map } from 'rxjs/operators';

class MockStoreService {
  private stateSubject = new BehaviorSubject<AppState>({
    user: {
      name: undefined,
      favourites: []
    },
    uiState: {
      theme: THEMES.LIGHT,
      pending: false,
      disableEverything: false
    }
  });

  theme$ = this.stateSubject.asObservable().pipe(
    map(state => state.uiState.theme)
  );

  setState(newState: Partial<AppState>, actionName: string): void {
    const currentState = this.stateSubject.value;
    const updatedState = { ...currentState, ...newState };
    this.stateSubject.next(updatedState);
  }
}

describe('ThemeChangerComponent', () => {
  let component: ThemeChangerComponent;
  let fixture: ComponentFixture<ThemeChangerComponent>;
  let storeService: MockStoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ThemeChangerComponent],
      imports: [
        MatIconModule,
        NoopAnimationsModule
      ],
      providers: [
        { provide: StoreService, useClass: MockStoreService }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeChangerComponent);
    component = fixture.componentInstance;
    storeService = TestBed.inject(StoreService) as unknown as MockStoreService;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a theme observable', (done) => {
    component.theme$.subscribe(theme => {
      expect(theme).toBe(THEMES.LIGHT);
      done();
    });
  });

  it('should change theme from LIGHT to DARK', () => {
    spyOn(storeService, 'setState').and.callThrough();

    component.changeTheme(THEMES.LIGHT);

    expect(storeService.setState).toHaveBeenCalledWith(
      { uiState: { theme: THEMES.DARK } } as Partial<AppState>,
      '[CHANGE THEME]'
    );
  });

  it('should change theme from DARK to LIGHT', () => {
    storeService.setState({ uiState: { theme: THEMES.DARK } } as Partial<AppState>, '[CHANGE THEME]');

    spyOn(storeService, 'setState').and.callThrough();

    component.changeTheme(THEMES.DARK);

    expect(storeService.setState).toHaveBeenCalledWith(
      { uiState: { theme: THEMES.LIGHT } } as Partial<AppState>,
      '[CHANGE THEME]'
    );
  });
});
