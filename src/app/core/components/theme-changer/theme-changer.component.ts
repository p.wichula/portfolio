import { ChangeDetectionStrategy, Component } from '@angular/core';
import { StoreService } from "../../services/store.service";
import { THEMES } from "../../../shared/enums/themes.enum";
import { AppState } from "../../../shared/models/app-state.model";
import { Observable } from "rxjs";

@Component({
  selector: 'app-theme-changer',
  templateUrl: './theme-changer.component.html',
  styleUrls: ['./theme-changer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemeChangerComponent {
  theme$: Observable<THEMES> = this._store.theme$;

  constructor(private _store: StoreService) {
  }

  changeTheme(currentTheme: THEMES): void {
    const newTheme = currentTheme === THEMES.LIGHT ? THEMES.DARK : THEMES.LIGHT;
    const newState = { uiState: { theme: newTheme } } as Partial<AppState>

    this._store.setState(newState, '[CHANGE THEME]');
  }
}
