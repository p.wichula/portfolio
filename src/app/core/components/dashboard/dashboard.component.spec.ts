import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardComponent } from './dashboard.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { of, throwError } from 'rxjs';
import { LyricsApiService } from '../../services/lyrics-api.service';
import { LyricsResponse } from '../../../shared/models/lyrics-response.model';
import { HttpErrorResponse } from '@angular/common/http';
import { ERROR_RESPONSES } from '../../../shared/enums/error-responses.enum';

class MockLyricsApiService {
  getLyrics$(artistName: string, songTitle: string) {
    if (artistName === 'valid' && songTitle === 'valid') {
      return of({ lyrics: 'Sample lyrics' } as LyricsResponse);
    } else {
      return throwError(new HttpErrorResponse({ error: { error: ERROR_RESPONSES.LYRICS_ERROR } }));
    }
  }
}

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let lyricsApiService: MockLyricsApiService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [ReactiveFormsModule],
      providers: [
        FormBuilder,
        { provide: LyricsApiService, useClass: MockLyricsApiService }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    lyricsApiService = TestBed.inject(LyricsApiService) as unknown as MockLyricsApiService;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form with default values', () => {
    expect(component.form.value).toEqual({ artistName: null, songTitle: null });
  });

  it('should search for lyrics and update lyrics$ and lyricsFound$', () => {
    const artistName = component.form.controls['artistName'] as any
    const songTitle = component.form.controls['songTitle'] as any

    artistName.setValue('valid');
    songTitle.setValue('valid');
    component.searchForLyrics();

    component.lyrics$?.subscribe(lyrics => {
      expect(lyrics).toBe('Sample lyrics');
    });

    expect(component.lyricsFound$.value).toBeTrue();
  });

  it('should handle error when searching for lyrics', () => {
    component.form.setValue({ artistName: null, songTitle: null });
    component.searchForLyrics();

    component.lyrics$?.subscribe(lyrics => {
      expect(lyrics).toBe(ERROR_RESPONSES.LYRICS_ERROR);
    });

    expect(component.lyricsFound$.value).toBeFalse();
  });

  it('should clear lyrics and reset lyricsFound$', () => {
    component.clearLyrics();

    expect(component.lyrics$).toBeUndefined();
    expect(component.lyricsFound$.value).toBeFalse();
  });

  it('should mark all form controls as touched on search', () => {
    spyOn(component.form, 'markAllAsTouched');
    component.searchForLyrics();
    expect(component.form.markAllAsTouched).toHaveBeenCalled();
  });

  it('should clean up on destroy', () => {
    spyOn(component['_destroy$'], 'next');
    spyOn(component['_destroy$'], 'complete');
    component.ngOnDestroy();
    expect(component['_destroy$'].next).toHaveBeenCalled();
    expect(component['_destroy$'].complete).toHaveBeenCalled();
  });
});
