import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { BehaviorSubject, catchError, map, Observable, of, Subject, tap } from "rxjs";
import { LyricsApiService } from "../../services/lyrics-api.service";
import { SUPPORTIVE_TEXTS } from "../../../shared/enums/supportive-texts.enum";
import { HttpErrorResponse } from "@angular/common/http";
import { LyricsResponse } from "../../../shared/models/lyrics-response.model";
import { ERROR_RESPONSES } from "../../../shared/enums/error-responses.enum";

const startForm = {
  artistName: [null, Validators.required],
  songTitle: [null, Validators.required],
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnDestroy {
  lyrics$?: Observable<string>;
  lyricsFound$ = new BehaviorSubject<boolean>(false);

  form = this._formBuilder.group(startForm);

  private readonly _destroy$ = new Subject<void>();
  protected readonly SUPPORTIVE_TEXTS = SUPPORTIVE_TEXTS;

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  constructor(
    private _lyricsService: LyricsApiService,
    private _formBuilder: FormBuilder
    ) {
  }

  /**
   * Searches for lyrics of a song specified by the user through the form controls.
   * If the form inputs are valid and the lyrics are found, they are streamed through the this.lyrics$ observable.
   * Otherwise, an error message is streamed.
   *
   * The results are processed by replacing newline character sequences with HTML breaks.
   * If an error occurs during HTTP request, it's error message will be returned instead.
   *
   * A separate observable this.lyricsFound$ is also updated to reflect whether the lyrics were found or not.
   */
  searchForLyrics(): void {
    const artistName = this.form.controls['artistName'].value;
    const songTitle = this.form.controls['songTitle'].value;

    this.form.markAllAsTouched();

    if (this.form.valid && artistName && songTitle) {
      this.lyrics$ = this._lyricsService.getLyrics$(artistName, songTitle).pipe(
        map((res: LyricsResponse) => {
          return res.lyrics.replace(/(\r\n|\n|\r)/gm, '<br>')
        }),
        catchError((err: HttpErrorResponse) => {
          return of(err.error.error);
        }),
        tap((value: string) => {
          const errorLyricsResponse = ERROR_RESPONSES.LYRICS_ERROR;

          if (value.length && value !== errorLyricsResponse) {
            this.lyricsFound$.next(true);
          } else {
            this.lyricsFound$.next(false);
          }
        }),
      )
    }
  }

  clearLyrics() {
    this.lyrics$ = undefined;
    this.lyricsFound$.next(false);
  }
}
