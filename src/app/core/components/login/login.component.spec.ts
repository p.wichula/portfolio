import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { StoreService } from '../../services/store.service';
import { LoginComponent } from './login.component';
import { AppState } from "../../../shared/models/app-state.model";

class MockStoreService {
  setState(subState: Partial<AppState>, actionName: string): void {}
}

class MockRouter {
  navigate(path: string[]): void {}
}


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let storeService: MockStoreService;
  let router: MockRouter;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: StoreService, useClass: MockStoreService },
        { provide: Router, useClass: MockRouter }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    storeService = TestBed.inject(StoreService) as unknown as MockStoreService;
    router = TestBed.inject(Router) as unknown as MockRouter;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form with default values', () => {
    expect(component.form.value).toEqual({ name: '' });
  });

  it('should have the correct validation for the name field', () => {
    const nameControl = component.form.controls['name'];
    expect(nameControl.valid).toBeFalsy();
    expect(nameControl.errors?.['required']).toBeTruthy();

    nameControl.setValue('ab');
    expect(nameControl.errors?.['minlength']).toBeTruthy();

    nameControl.setValue('a'.repeat(16));
    expect(nameControl.errors?.['maxlength']).toBeTruthy();

    nameControl.setValue('validName');
    expect(nameControl.valid).toBeTruthy();
  });

  it('should call setState and navigate when form is valid and submitted', () => {
    spyOn(storeService, 'setState');
    spyOn(router, 'navigate');

    component.form.controls['name'].setValue('validName');
    component.submit();

    expect(storeService.setState).toHaveBeenCalledWith({ user: { name: 'validName' } } as Partial<AppState>, '[SETTING NAME]');
    expect(router.navigate).toHaveBeenCalledWith(['/dashboard']);
  });

  it('should not call setState or navigate when form is invalid', () => {
    spyOn(storeService, 'setState');
    spyOn(router, 'navigate');

    component.form.controls['name'].setValue('');
    component.submit();

    expect(storeService.setState).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();
  });
});
