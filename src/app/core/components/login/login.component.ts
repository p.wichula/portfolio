import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { SUPPORTIVE_TEXTS } from "../../../shared/enums/supportive-texts.enum";
import { StoreService } from "../../services/store.service";
import { AppState } from "../../../shared/models/app-state.model";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
  form = this._formBuilder.group({
    name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
  });

  protected readonly SUPPORTIVE_TEXTS = SUPPORTIVE_TEXTS;

  constructor(private _formBuilder: FormBuilder, private _store: StoreService, private _router: Router) {
  }

  submit(): void {
    const name = this.form.controls['name'].value;

    this.form.markAllAsTouched();

    if (this.form.valid && name) {
      // TODO - ADD ENUM FOR EXISTING STORE ACTIONS
      this._store.setState({ user: { name } } as Partial<AppState>, '[SETTING NAME]');
      this._router.navigate(['/dashboard'])
    }
  }
}
