import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { LyricsResponse } from "../../shared/models/lyrics-response.model";

@Injectable({
  providedIn: 'root'
})
export class LyricsApiService {

  constructor(private _httpClient: HttpClient) {
  }

  getLyrics$(artistName: string, songTitle: string): Observable<LyricsResponse> {
    const url = `https://api.lyrics.ovh/v1/${artistName}/${songTitle}`;

    return this._httpClient.get<LyricsResponse>(url);
  }
}
