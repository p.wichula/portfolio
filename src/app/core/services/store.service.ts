import { Injectable } from '@angular/core';
import { BehaviorSubject, distinctUntilChanged, map, Observable } from "rxjs";
import { AppState } from "../../shared/models/app-state.model";
import { THEMES } from "../../shared/enums/themes.enum";

const initialAppState: AppState = {
  /** User data **/
  user: {
    name: undefined,
    favourites: []
  },
  /** Ui state stands for variables that change UI like loaders etc. **/
  uiState: {
    theme: THEMES.LIGHT,
    pending: false,
    disableEverything: false
  }
}

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private readonly _store$ = new BehaviorSubject(initialAppState);

  /** For debugging purposes we implement actionHistory if you need to use it for some feature,
   * create a special variables for it **/
  actionHistory: { actionName: string, state: AppState }[] = [];

  /**
   * Updates the current state with a partial AppState object and logs the action.
   *
   * This method merges the current state with the provided subState, and updates
   * the store with this new combined state.
   * It also logs the action that caused the state change in the actionHistory.
   *
   * @param {Partial<AppState>} subState - Object containing the app's substate to be
   * merged with the current state.
   * @param {string} actionName - The identifier name of the action that caused
   * the state change.
   * @returns {void}
   */
  setState(subState: Partial<AppState>, actionName: string): void {
    const currentState = this._store$.value;
    const newState: AppState = { ...currentState, ...subState };

    this.actionHistory.push({ actionName, state: newState });
    this._store$.next(newState);
  }

  /**
   * Observable that emits 'pending' state from the `uiState`.
   *
   * This observable uses the `store`'s state, mapping it to get only the `pending`
   * status from the `uiState` and emits distinct values until
   * the 'pending' status changes.
   *
   * @type {Observable<boolean>}
   **/
  pending$: Observable<boolean> = this._store$.pipe(
    map((state) => state.uiState.pending),
    distinctUntilChanged()
  )

  theme$: Observable<THEMES> = this._store$.pipe(
    map((state) => state.uiState.theme),
    distinctUntilChanged()
  )

  getState$(): Observable<AppState> {
    return this._store$.asObservable();
  }

  getState(): AppState {
    return this._store$.value;
  }
}
