import { TestBed } from '@angular/core/testing';
import { StoreService } from './store.service';
import { AppState } from '../../shared/models/app-state.model';
import { THEMES } from '../../shared/enums/themes.enum';
import { take } from 'rxjs/operators';

describe('StoreService', () => {
  let service: StoreService;
  const initialAppState: AppState = {
    user: {
      name: undefined,
      favourites: []
    },
    uiState: {
      theme: THEMES.LIGHT,
      pending: false,
      disableEverything: false
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have initial state', (done) => {
    service.getState$().pipe(take(1)).subscribe(state => {
      expect(state).toEqual(initialAppState);
      done();
    });
  });

  it('should update state and log action', () => {
    const newState: Partial<AppState> = {
      user: {
        name: 'John Doe',
        favourites: ['item1']
      }
    };
    const actionName = 'Update User';

    service.setState(newState, actionName);

    const expectedState = { ...initialAppState, ...newState };
    expect(service.getState()).toEqual(expectedState);
    expect(service.actionHistory.length).toBe(1);
    expect(service.actionHistory[0]).toEqual({ actionName, state: expectedState });
  });

  it('should emit new pending state', (done) => {
    const newState: Partial<AppState> = {
      uiState: {
        pending: true
      }
    } as  Partial<AppState>;
    const actionName = 'Set Pending';

    service.pending$.pipe(take(2)).subscribe((pending) => {
      if (pending) {
        expect(pending).toBeTrue();
        done();
      }
    });

    service.setState(newState, actionName);
  });

  it('should emit new theme state', (done) => {
    const newState: Partial<AppState> = {
      uiState: {
        theme: THEMES.DARK
      }
    } as  Partial<AppState>;
    const actionName = 'Set Theme';

    service.theme$.pipe(take(2)).subscribe((theme) => {
      if (theme === THEMES.DARK) {
        expect(theme).toBe(THEMES.DARK);
        done();
      }
    });

    service.setState(newState, actionName);
  });
});
