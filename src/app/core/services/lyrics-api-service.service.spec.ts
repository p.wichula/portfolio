import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { LyricsApiService } from './lyrics-api.service';
import { LyricsResponse } from '../../shared/models/lyrics-response.model';

describe('LyricsApiService', () => {
  let service: LyricsApiService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LyricsApiService]
    });
    service = TestBed.inject(LyricsApiService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve lyrics for given artist and song title', () => {
    const mockResponse: LyricsResponse = { lyrics: 'Sample lyrics' };
    const artistName = 'artist';
    const songTitle = 'song';

    service.getLyrics$(artistName, songTitle).subscribe((response) => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpTestingController.expectOne(`https://api.lyrics.ovh/v1/${artistName}/${songTitle}`);
    expect(req.request.method).toBe('GET');
    req.flush(mockResponse);
  });

  it('should handle HTTP errors gracefully', () => {
    const errorMessage = 'Not Found';
    const artistName = 'invalidArtist';
    const songTitle = 'invalidSong';

    service.getLyrics$(artistName, songTitle).subscribe({
      next: () => fail('expected an error, not lyrics'),
      error: (error) => {
        expect(error.status).toBe(404);
        expect(error.statusText).toBe(errorMessage);
      }
    });

    const req = httpTestingController.expectOne(`https://api.lyrics.ovh/v1/${artistName}/${songTitle}`);
    expect(req.request.method).toBe('GET');
    req.flush({ errorMessage }, { status: 404, statusText: errorMessage });
  });
});
